import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'


import './index.scss'

const Header = () => (
  <div
    style={{
      background: 'transparent',
      marginBottom: '0.45rem',
      height: '24vh'
    }}
  >
        <div id="rootLink"><Link
          to="/"
        >
         TIM  GLOVER<br/> f i l m m a k e r   
        </Link></div>

    <div id="topper" class='topMin'>

        <div id="topLink"><Link
          to="/film">
          film
        </Link></div>
      
        <div id="topLink"><Link
          to="/commercialVideo">
           commercials and music videos
        </Link></div>

        <div id="topLink"><Link
          to="/photography">
          photography
        </Link></div>

        <div id="topLink"><Link
          to="/contact">
           contact
        </Link></div>
    </div>
  </div>
)

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Tim Glover Film"
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <Header />
    <div
      style={{
        margin: '0 auto',
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    > 
      {children()}
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
