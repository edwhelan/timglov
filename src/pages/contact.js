import React from 'react'
import Link from 'gatsby-link'
import bgImg from './capsuleCopy.jpg'
import { Image } from 'cloudinary-react';

const ContactPage = () => (
  <div id="bodyContainer">
  <div id="contactDiv">

  <img id='avatar' src="https://res.cloudinary.com/dnded1/image/upload/v1514570941/me_fkgnzd.jpg"/><br/><br/>
    <p>For all serious inquiries for film projects or photoshoots, Tim can be reached at <br/>
    Email:<a href="mailto:timglover31@gmail.com" className="email"> timglover31@gmail.com</a><br/>
    Twitter: <a href=" https://twitter.com/film_atic">https://twitter.com/film_atic</a> <br/>
    Instagram: <a href="https://www.instagram.com/film_atic"> https://www.instagram.com/film_atic </a>
         </p>

         <div id = "containerDiv"><a id='indexBG' href='/bts'><img src={bgImg} alt="" /></a></div>
    {/* <Link to="/">Go back to the homepage</Link> */}
  </div>
  </div>
)

export default ContactPage
