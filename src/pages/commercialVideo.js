import React from 'react'
import Link from 'gatsby-link'
import bgImg from './capsuleCopy.jpg';


const CommercialVideo = () => (
  <div id="bodyContainer">
    <p>
    <iframe src="https://player.vimeo.com/video/274087003?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
    <p>A Francois Wedding<br/>
    (Wedding)</p>
    <iframe src="https://player.vimeo.com/video/265888491?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
    <p>The Dround Hounds - Cut Me Loose <br/>
    (Music Video)</p> 
    <iframe src="https://player.vimeo.com/video/260800547?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
    <p>Red Hare Promo <br/>
    (Commercial)</p>
    <iframe src="https://player.vimeo.com/video/259279886?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
    <p>Steady Common - Fly(my heart, my head) <br/>
    (Music Video)</p>

      <div id='youtubePlayer'>
    <iframe src="https://www.youtube.com/embed/Vv_H4dvkUGI?rel=0&amp;showinfo=0" width="640" height="360" frameBorder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
    </div>
    <br/>
    <p>Matthew Parker - Never Giving Up on You <br/>
    (Music Video)</p>

    <div id='youtubePlayer'>
    <iframe src="https://www.youtube.com/embed/5qL4xfZmq30?rel=0&amp;showinfo=0" id='ytVid' width="640" height="360" frameBorder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> 
    </div>
    <br/>
    <p>Starbound PR - Who We Are <br/>
    (Commercial)</p>

    <iframe src="https://player.vimeo.com/video/249406245?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
    <p>Good For Education, Bad for Health <br/>
    (Commercial)</p>

    <iframe src="https://player.vimeo.com/video/241064997?color=ff0179&title=0&byline=0&portrait=0" width="640" height="346" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
    <p>Giang - Best For Me<br/>
    (Music Video)</p>

    <iframe src="https://player.vimeo.com/video/224505818?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
    <p>Julie & Steve | Tennesse <br/>
    (Commercial)</p>
      
    <iframe src="https://player.vimeo.com/video/193648917?color=ff0179&title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
    <p>Cargado Energy <br/>
    (Commercial)</p>
      </p>

      <div id = "containerDiv"><a id='indexBG' href='/bts'><img src={bgImg} alt="" /></a></div>
  </div>   
)

export default CommercialVideo
