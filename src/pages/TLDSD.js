import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

//Images from TLDSD set

const photos = [
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481284/TLDSD/DSC00727_mhfrtm.jpg', width: 5, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481282/TLDSD/DSC00746_f7zthx.jpg', width: 5, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481281/TLDSD/DSC00523_fotpb3.jpg', width: 5, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481280/TLDSD/DSC00536_eijbfq.jpg', width: 5, height: 3},
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481270/TLDSD/DSC00317_rbw9kp.jpg', width: 5, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481269/TLDSD/DSC00677_n7vzoe.jpg', width: 5, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1514481265/TLDSD/DSC00655_jdc92a.jpg', width: 5, height: 3 },
    { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481263/TLDSD/DSC00657_sbnwis.jpg',
     width:5,  height:3 },
     { src:
     'https://res.cloudinary.com/dnded1/image/upload/v1514481261/TLDSD/DSC00653_ugbhod.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481260/TLDSD/DSC00540_xebku2.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481256/TLDSD/DSC00422_ntt74j.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481256/TLDSD/DSC00463_pjspsc.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481253/TLDSD/DSC00432_v4xeh9.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481251/TLDSD/DSC00326_icyzpo.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481240/TLDSD/DSC00435_gcnlq1.jpg',
     width:5,  height:3 },
     { src:
    'https://res.cloudinary.com/dnded1/image/upload/v1514481219/TLDSD/DSC00328_u33vrq.jpg',
     width:5,  height:3 },
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915245/tldsd/DSC00216.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915229/tldsd/DSC00644.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915227/tldsd/DSC00616.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915246/tldsd/DSC00593.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915239/tldsd/DSC00592.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915243/tldsd/DSC00554.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915242/tldsd/DSC00582.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915224/tldsd/DSC00274.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915247/tldsd/DSC00409.jpg', width: 5, height: 3},
     {src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514915223/tldsd/DSC00283.jpg', width: 5, height: 3}
  ];



// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


const TLDSDPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default TLDSDPage
