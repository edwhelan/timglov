import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    {src: 'https://res.cloudinary.com/dnded1/image/upload/v1529593838/headshots/DSC03110.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593832/headshots/DSC03064.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593830/headshots/DSC02994.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593826/headshots/DSC03009.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593838/headshots/DSC03189.jpg', width: 2, height: 3 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593837/headshots/DSC03265.jpg', width: 2, height: 3 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593837/headshots/DSC03206.jpg', width: 2, height: 3 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593833/headshots/DSC03183.jpg', width: 2, height: 3 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593829/headshots/DSC03240.jpg', width: 2, height: 3 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/v1529593828/headshots/DSC03217.jpg', width: 2, height: 3 }
  ];


const HeadshotsPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default HeadshotsPage