import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757615/sbs/DSC00873_fpiczo.jpg', width: 3, height: 2 },
    {src:'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757629/sbs/DSC00821_gni8md.jpg', width: 3, height: 2},
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757623/sbs/DSC00822_h8spl6.jpg', width: 3, height: 2},
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757616/sbs/DSC00827_kvag63.jpg', width: 3, height: 2},
    {src:'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757624/sbs/DSC00859_gwkr3p.jpg', width: 3, height: 2},
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757618/sbs/DSC00863_2_dbwfog.jpg', width: 3, height: 2},
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757630/sbs/DSC00895_bcsrib.jpg', width: 3, height: 2},
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514757621/sbs/DSC00909_bclpjy.jpg', width: 3, height: 2}
  ];


const SBSPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default SBSPage