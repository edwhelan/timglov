import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897830/g4eb4h/DSC01385.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897826/g4eb4h/DSC01394.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897822/g4eb4h/DSC01446.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897836/g4eb4h/DSC01474.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897866/g4eb4h/DSC01491.jpg', width: 4, height: 3 },
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897863/g4eb4h/DSC01513.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897850/g4eb4h/DSC015222.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897881/g4eb4h/DSC01551-2.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897822/g4eb4h/DSC01559.jpg', width: 4, height: 3 },
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897822/g4eb4h/DSC01559.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897848/g4eb4h/DSC01560.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897860/g4eb4h/DSC01571.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897879/g4eb4h/DSC01574.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897888/g4eb4h/DSC01586-2.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897887/g4eb4h/DSC01588.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897886/g4eb4h/DSC01575.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897888/g4eb4h/DSC01590.jpg', width: 4, height: 3 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1516897822/g4eb4h/DSC01666.jpg', width: 4, height: 3 }
  ];


const G4EB4HPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default G4EB4HPage
