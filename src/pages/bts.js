import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848208/bts/BTS_Cantil_1_guoix8.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848250/bts/BTS_Cantil_2_zas7qs.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848258/bts/BTS_Cantil_3_qmqwkg.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848226/bts/BTS_Cantil_4_s69nnk.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848248/bts/BTS_Cantil_5_y0c2ll.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848247/bts/BTS_Cantil_6_ussb9t.jpg', width: 3, height: 2 },
      { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848211/bts/BTS_N-Touch_1_brzcuw.jpg', width: 2, height: 3 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848210/bts/BTS_N-Touch_2_x5uztj.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848210/bts/BTS_N-Touch_3_t245pe.jpg', width: 3, height: 2 },
      { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848209/bts/BTS_Out_Of_Here_1_ayqp9d.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848209/bts/BTS_N-Touch_4_jgie3d.jpg', width: 2, height: 3 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848213/bts/BTS_Out_Of_Here_2_duajtj.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848214/bts/BTS_Out_Of_Here_3_uytrkq.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848219/bts/BTS_Pie_1_cmk6lr.jpg', width: 3, height: 2 },
      { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514848218/bts/BTS_Stranded_bzku92.jpg', width: 2, height: 3 }
  ];


const BTSPage = () => (
  <div id='app'>
  <App/>
   </div>
)

export default BTSPage