import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514764873/Villain/DSC01302_khw8tq.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514764842/Villain/DSC01210_zzuvjx.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514764863/Villain/DSC01227_jg3noc.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514764860/Villain/DSC01232_xqvtj0.jpg', width: 3, height: 2 },
    {src:'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514764862/Villain/DSC01262_jmko1v.jpg', width: 3, height: 2 }
  ];


const VillainPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default VillainPage