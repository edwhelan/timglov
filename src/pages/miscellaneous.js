import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755071/Miscellaneous/20170420-DSC00059_j85kxg.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755062/Miscellaneous/20170428-DSC00282_xypfbx.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755062/Miscellaneous/20170429-DSC00402_qq0bgy.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755054/Miscellaneous/20170516-DSC00977_j1aney.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755035/Miscellaneous/DSC00020_u2raot.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755033/Miscellaneous/DSC00056_1_mtxgqc.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755036/Miscellaneous/DSC00066_da3h8n.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755066/Miscellaneous/DSC00084_2_ryeux6.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755075/Miscellaneous/DSC01802_lj3ipx.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755103/Miscellaneous/DSC01807_ocwifk.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755105/Miscellaneous/DSC01808_srxivj.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755110/Miscellaneous/DSC01903_bh7t2l.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755106/Miscellaneous/DSC01905_y0urkd.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755108/Miscellaneous/DSC01906_ehr6k0.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514755106/Miscellaneous/DSC01909-2_tcyxhw.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1518381904/Miscellaneous/DSC00198.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1518381904/Miscellaneous/DSC00019.jpg', width: 3, height: 2 },
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1518381902/Miscellaneous/tumblr_op1khjjilh1taww7ao4_1280.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1518381903/Miscellaneous/tumblr_op1khjjilh1taww7ao6_1280.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1518381902/Miscellaneous/tumblr_op1khjjilh1taww7ao2_1280.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594604/Miscellaneous/DSC02677.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594609/Miscellaneous/DSC02715.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594613/Miscellaneous/DSC02686.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594614/Miscellaneous/DSC02890.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594614/Miscellaneous/DSC02710.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594614/Miscellaneous/DSC02886.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594618/Miscellaneous/DSC02688.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594647/Miscellaneous/DSC02420.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594664/Miscellaneous/DSC02672.jpg', width: 3, height: 2 }

  ];


const MiscellaneousPage = () => (
  <div id="bodyContainer">  
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default MiscellaneousPage