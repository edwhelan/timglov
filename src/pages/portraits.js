import React from 'react'
import Link from 'gatsby-link'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'
import Lightbox from 'react-images'

// Lightbox and photo
class App extends React.Component {
    constructor() {
      super();
      this.state = { currentImage: 0 };
      this.closeLightbox = this.closeLightbox.bind(this);
      this.openLightbox = this.openLightbox.bind(this);
      this.gotoNext = this.gotoNext.bind(this);
      this.gotoPrevious = this.gotoPrevious.bind(this);
    }
    openLightbox(event, obj) {
      this.setState({
        currentImage: obj.index,
        lightboxIsOpen: true,
      });
    }
    closeLightbox() {
      this.setState({
        currentImage: 0,
        lightboxIsOpen: false,
      });
    }
    gotoPrevious() {
      this.setState({
        currentImage: this.state.currentImage - 1,
      });
    }
    gotoNext() {
      this.setState({
        currentImage: this.state.currentImage + 1,
      });
    }
    render() {
      return (
        <div>
          <Gallery photos={photos} onClick={this.openLightbox} />
          <Lightbox images={photos}
            onClose={this.closeLightbox}
            onClickPrev={this.gotoPrevious}
            onClickNext={this.gotoNext}
            currentImage={this.state.currentImage}
            isOpen={this.state.lightboxIsOpen}
          />
        </div>
      )
    }
  }


  const photos = [
    { src: 'http://res.cloudinary.com/dnded1/image/upload/q_auto/v1514753012/Portraits/DSC00340_pv8yhf.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514752996/Portraits/DSC00668_2_erdfa9.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514752985/Portraits/DSC00672_pr1rly.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514753005/Portraits/DSC00699_blftdr.jpg', width: 3, height: 2 },
    {src:
    'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514753012/Portraits/DSC02192_wt07si.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/q_auto/v1514753011/Portraits/DSC00714_jdsd91.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594273/Portraits/DSC02595.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594272/Portraits/DSC02604.jpg', width: 3, height: 2 },
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594272/Portraits/DSC03347.jpg', width: 3, height: 2 },
    
    { src: 'https://res.cloudinary.com/dnded1/image/upload/v1529594271/Portraits/DSC03358.jpg', width: 3, height: 2 }
  ];


const PortraitPage = () => (
  <div id="bodyContainer">
  <div id='app'>
  <App/>
   </div>
   </div>
)

export default PortraitPage
