import React from 'react'
import Link from 'gatsby-link'
import {Image} from 'cloudinary-react'
import bgImg from './capsuleCopy.jpg'
import {render} from 'react-dom'
import Gallery from 'react-photo-gallery'





const PhotographyPage = () => (
  <div id="bodyContainer">
  <div id="photoGrid">

    <a href="/portraits">
      <img alt="portraits"         
    src="https://res.cloudinary.com/dnded1/image/upload/q_auto/v1515001232/title%20cards/Portaits_kvqidf.jpg"/></a>
    
    
    <a href="/weddings">
    <img alt="weddings"         
    src="https://res.cloudinary.com/dnded1/image/upload/q_auto/v1515001233/title%20cards/Weddings_ercox7.jpg"/></a>
    
    <a href="/miscellaneous">
    <img alt="miscellaneous"         
    src="https://res.cloudinary.com/dnded1/image/upload/q_auto/v1515001233/title%20cards/Miscellaneous_jp4eth.jpg"/></a>
    
    <a href="/shoots">
    <img alt="shoots"     
    src="https://res.cloudinary.com/dnded1/image/upload/q_auto/v1515005649/title%20cards/Shoots.jpg"/></a>

    <a href="/headshots">
    <img alt="headshots"     
    src="https://res.cloudinary.com/dnded1/image/upload/v1529593822/headshots/Headshotstitle.jpg"/></a>
  </div>
  </div>
)





export default PhotographyPage
