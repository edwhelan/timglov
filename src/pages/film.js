import React from 'react'
import Link from 'gatsby-link'
import bgImg from './capsuleCopy.jpg';

const FilmPage = () => (
  <div id="bodyContainer">
  <div id="filmDiv">
    
  {/* <img  id="pillBG" src={bgImg} alt="" /> */}
  <iframe src="https://player.vimeo.com/video/254546910?title=0&byline=0&portrait=0" width="640" height="346" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
  <p>To Live & Die in San Diego</p><br/>

  <iframe src="https://player.vimeo.com/video/225516904?title=0&byline=0&portrait=0" width="640" height="268" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/>
  <p>Cantil</p><br/>

  <iframe src="https://player.vimeo.com/video/187611158?title=0&byline=0&portrait=0" width="640" height="270" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
  <p>Out of Here</p><a href='https://www.youtube.com/watch?v=OwoaFJv7FwM&feature=youtu.be'> <p>Full Film </p></a><br/>
    
    
  <iframe src="https://player.vimeo.com/video/159886982?title=0&byline=0&portrait=0" width="640" height="270" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><br/>
  <p>N-Touch </p><a href='https://www.youtube.com/watch?v=EwKML5PotpU'><p>Full Film</p></a> <br/>
     
    
  <iframe src="https://player.vimeo.com/video/110792831?title=0&byline=0&portrait=0" width="640" height="360" frameBorder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <br/><p>Stranded on Centurion</p><a href='https://www.youtube.com/watch?v=jv072IY-bjY'> <p>Full Film </p></a><br/>


  <div id = "containerDiv"><a id='indexBG' href='/bts'><img src={bgImg} alt="" /></a></div>
    
    {/* <Link to="/">Go back to the homepage</Link> */}
  </div>
  </div>
)

export default FilmPage
