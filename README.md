rarecapsule.com
Portfolio site for Tim Glover

Made With Gatsby.js

Used Plugins :
Favicon Injector, 
https://github.com/Creatiwity/gatsby-plugin-favicon
for adding favicon easily

react-photo-gallery,
https://github.com/neptunian/react-photo-gallery
-for photo grid
react-images , 
https://github.com/jossmac/react-images
-for lightbox effect
```

## Deploy

"# rarecapsule" 
"# rarecapsule" 
"# RareCapsule" 
